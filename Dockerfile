FROM tomcat:9.0

# Create a directory in the image to store the WAR file
WORKDIR /usr/local/tomcat/webapps

# Copy the WAR file from the local build context to the image
COPY target/petclinic.war .

# Expose port 8080 (Tomcat's default port)
EXPOSE 8080
